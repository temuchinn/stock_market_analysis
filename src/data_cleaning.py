import pandas as pd
import numpy as np


def preproccess_stock_data(
    X: pd.DataFrame,
    lags: int,
    log_ret: bool = True,
    drop_na: bool = True,
    drop_index: bool = False,
) -> pd.DataFrame:
    # making up the names for features in the dataset
    # y_i - target value with lag i
    # y - target value to be predicted
    # hello moto moto

    X_new = X.copy()
    colnames = [f"y_{lag}" if lag > 0 else "y" for lag in range(lags + 1)]
    if log_ret:
        X_new = np.log(X_new) - np.log(X_new.shift(1))

    X_new = pd.concat(
        [X_new.shift(lag) for lag in range(lags + 1)], axis=1, keys=colnames
    )

    # when we do shift, there are omissions in the objects at the beginning
    # so, if we set drop_na flag, we will drop such objects
    if drop_na:
        # last feature has the largest number of gaps at the beginning
        # so first_valid_index is defined by this column
        first_idx = X_new.iloc[:, -1].first_valid_index()
        X_new = X_new.loc[first_idx:]
    return X_new
