import pandas as pd
import yfinance as yf


def download_stock_data(
    ticker: str, period: str, interval: str, info_type: str | list[str] = "Close"
) -> pd.Series:
    tick = yf.Ticker(ticker)
    X = tick.history(period=period, interval=interval)[info_type]
    return X


if __name__ == "__main__":
    print("Hello")
